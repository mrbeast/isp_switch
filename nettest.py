#from threading import Thread
#import threading
from logger import PSCLogger
import time
import os
import re
from datetime import datetime
import locale
import subprocess
import configparser
#=======================================================================================================

logger = PSCLogger( 'testing' )
logger.start()

current_dir = os.getcwd() + '/'
config = configparser.RawConfigParser()
config.optionxform = lambda option: option
config.read( current_dir + 'conf/nets.conf')
CheckHost = config['main']['CheckHost']

GWmain = config['main']['GWmain']
NAmain = config['main']['NAmain']
GWres = config['main']['GWres']
NAres = config['main']['NAres']
MaxLoss = config['main']['MaxLoss']
#def start_check():



while True:
        
    #Запоминаем текущий адаптер на котором прописан маршрут по умолчанию
    EthDo = subprocess.check_output("route -n | grep 0.0.0.0 | awk 'NR==1 {print $8}' | tr -d '\n'",shell=True, universal_newlines=True)
    
    # Добавляем временный точный маршрут до контрольного адреса через основного провайдера
    subprocess.Popen('route add {0}/32 gw {1}'.format(CheckHost, GWmain), shell=True, stdout=subprocess.PIPE)
    
    # Проверяем контрольный адрес и запоминаем процент потерь
    ping = subprocess.check_output("ping -c10 -W3 -q {0} | grep 'packet loss'" .format(CheckHost), shell=True, universal_newlines=True)
    str_ping = re.finditer(r"\d+((.|,)\d+)?\%", ping)
    pgw = ''
    for v in str_ping:
        pgw = ping[v.span()[0]:v.span()[1]]
    if pgw.find("%") > -1:
        pgw = pgw[0:pgw.find("%")]


    if pgw != '0':
        logger.log(" - Потери пакетов до контрольного адреса " + CheckHost + " составили " + pgw + "%", "Info")

    subprocess.call('route del {0}/32 gw {1}'.format(CheckHost, GWmain), shell=True)

       
    if int(pgw) >= int(MaxLoss) and EthDo == NAmain:
        logger.log(" - Переход на резервный канал - шлюз " + GWres + ", интерфейс " + NAres + ".\
        Причина - потери пакетов на интерфесе " + NAmain + " до контрольного хоста " + CheckHost + " составили " + pgw +"% при пороге не более " + MaxLoss + "%", "Error")
        
        subprocess.call('route del default; route add default gw {0}'.format(GWres), shell=True)
                
        time.sleep(10)
        vpn_restart = subprocess.check_output('service openvpn restart', shell=True, universal_newlines=True)
        logger.log(vpn_restart, "Info")

    if EthDo == NAres and int(pgw) < int(MaxLoss):
        logger.log(" - Переход на основной канал - шлюз " + GWmain + ", интерфейс " + NAmain, "Info")
        subprocess.call('route del default; route add default gw {0}'.format(GWmain), shell=True)
        time.sleep(10)
        vpn_restart = subprocess.check_output('service openvpn restart', shell=True, universal_newlines=True)
        logger.log(vpn_restart, "Info")
    
    time.sleep(50)

#stat_thread = Thread( target=start_check, args=[] )
#stat_thread.start()
