[nets.conf]

CheckHost - permanently available external host that will be pinging to check Internet connection

GWmain - gateway of your main ISP.

NAmain - network adapter of your main ISP

GWres - gateway of your reserve ISP.

NAres - network adapter of your reserve ISP

MaxLoss - max loss in % (from 0 to 100)
